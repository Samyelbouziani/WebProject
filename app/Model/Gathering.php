<?php

App::uses('AppModel', 'Model');

class Gathering extends AppModel {

	function addGathering($ID, $date, $end_date, $location_name, $description, $type, $contest_id){ 
    	$this->create();
    	$this->save(array('member_id'=>$ID, 'date'=>$date, 'end_date'=>$end_date, 'location_name'=>$location_name, 'description'=>$description, 'type'=>$type, 'contest_id'=>$contest_id));
    }
}
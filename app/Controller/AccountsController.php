    
<?php 
    
App::uses('AppController', 'Controller');
    
/**
 * Main controller of our small application
 *
 * @author ...
 */
class AccountsController extends AppController
{ 
    public $uses = array('Member','Results', 'Gathering', 'Device', 'Log'); 
        
    /**
     * index method : first page
     *
     * @return void
     */
    public function moncompte()
    {
       $this->set('raw',$this->Member->findById(1));
           
           
    }
     public function myresults()
    {
        
    }
    public function index()
    {
       
    } 
      public function halloffame()  {
       $this->set('raw',$this->Member->find());
   }
 public function addmember(){
        if ($this->request->is('post'))       
{            pr($this->request->data); 
            $this->Member->addMember($this->request->data['memberadd']['Email'],$this->request->data['memberadd']['password']);
}
    
    }
public function modifymember(){
                    if ($this->request->is('post'))       
            { 	pr($this->request->data); 
    $this->Member->modifyMember($this->request->data['membermodify']['ID'], $this->request->data['membermodify']['Nouvel Email'], $this->request->data['membermodify']['Nouveau Password']);
            }
}

public function adddevice(){
                    if ($this->request->is('post'))       
            { 	pr($this->request->data); 
    $this->Device->addDevice($this->request->data['deviceadd']['ID du membre'], $this->request->data['deviceadd']['serial'], $this->request->data['deviceadd']['description'],  $this->request->data['deviceadd']['trusted']);
            }
}

public function validatedevice(){
                    if ($this->request->is('post'))       
            { 	pr($this->request->data); 
    $this->Device->validateDevice($this->request->data['devicevalidate']['ID du device à valider']);
            }
}

public function addgathering(){
                    if ($this->request->is('post'))       
            { 	pr($this->request->data); 
    $this->Gathering->addGathering($this->request->data['gatheringadd']['ID du membre'], $this->request->data['gatheringadd']['Date et heure de début'], $this->request->data['gatheringadd']['Date et heure de fin'],$this->request->data['gatheringadd']['Lieu'],$this->request->data['gatheringadd']['Description'],$this->request->data['gatheringadd']['Type'],$this->request->data['gatheringadd']['Contest ID']);
            }
}

public function addresults(){
                    if ($this->request->is('post'))       
            { 	pr($this->request->data); 
    $this->Results->addResults($this->request->data['resultsadd']['ID du membre'], $this->request->data['resultsadd']['ID du contest'], $this->request->data['resultsadd']['Points']);
            }
}

public function pageconnexion()
{

}
public function mesobjetsconnectes()
{

}
public function messeances()
{


}
                                    
}
?>